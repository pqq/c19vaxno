// 21.10.26 // frode burdal klevstul // klevstul.com
export class WwwService {
    constructor() { }
    getWebResource(url, callback) {
        // asyncronous load web resource
        function handleErrorResponse(responseText) {
            let responseJson = JSON.parse(responseText);
            let newEl = document.createElement('div');
            newEl.className = "error";
            let output = 'BUDDY, AN ERROR HAS OCCURRED!<br><br>';
            if (responseJson.error.code || responseJson.error.message) {
                output += responseJson.error.code + ' ' + responseJson.error.message;
            }
            else if (responseJson.error) {
                output += ' ' + responseJson.error;
            }
            newEl.innerHTML = output;
            document.body.appendChild(newEl);
        }
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                if (xhr.responseText.match("\"error\":")) {
                    handleErrorResponse(xhr.responseText);
                }
                else {
                    callback(xhr.responseText);
                }
            }
        };
        xhr.open('GET', url, true);
        xhr.send();
    }
}
