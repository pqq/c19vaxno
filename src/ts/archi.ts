// 21.10.26 // frode burdal klevstul // klevstul.com

/*
    3rd party sources:

  - DefinitivelyTyped
    - latest: for chart.js version 2.9 | https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/chart.js/index.d.ts
  - Chart.js
    - https://github.com/chartjs/Chart.js/releases
      - version 2.9.3 | https://github.com/chartjs/Chart.js/releases/tag/v2.9.3
*/

import { WwwService } from "./services/www.service.js";

let wwwService: WwwService = new WwwService();
let url_db = "https://pqq.gitlab.io/c19vaxno/db/database.json";
let default_language = "en";
let language = default_language;
let selected_language = sessionStorage.getItem('vax.language');

if (location.hostname === "localhost" || location.hostname === "127.0.0.1"){
    url_db = "./db/database.json";
}

if (selected_language !== null && selected_language !== undefined) {
    language = selected_language;
}

function getTextGivenLanguage(text_array:any, language:string) {
    let the_text = "NO_TEXT_FOUND";

    // get the title in the correct language
    for (let index in text_array) {
        if (text_array[index].language == language) {
            the_text = text_array[index].text
        }
    }

    return the_text;
}

function getDatasetGivenLanguage(dataset_array:any, language:string) {
    let the_dataset = [];

    for (let index in dataset_array) {
        for (let index2 in dataset_array[index]) {
            if (index2 == language) {
                the_dataset = dataset_array[index][index2];
            }
        }
    }

    return the_dataset;
}

function replaceElementWithString(element_id:string, new_string:string) {
    let dom_element = document.getElementById(element_id);
    if (dom_element) {
        dom_element.innerHTML = new_string;
    }
}

function drawLineChart(element_id:string, labels:any, datasets:any, stacked:boolean, spanGaps:boolean) {
    let chart_element = document.getElementById(element_id) as HTMLCanvasElement;
    if (chart_element) {
        let ctx = chart_element.getContext("2d");
        if (ctx) {
            var myChart = new Chart(ctx, {
                  type: "line"
                , data: {
                      labels: labels
                    , datasets: datasets
                },
                options: {
                    scales: {
                        yAxes: [
                            {
                                stacked: stacked
                            }
                        ]
                    }
                    , spanGaps: spanGaps
                }
            });
            // avoid "'myChart' is declared but its value is never read" error message
            myChart = myChart;
        }
    }
}

function drawBarOrPieChart(element_id:string, chart_type:string, labels:any, datasets:any, legend:boolean) {
    let chart_element = document.getElementById(element_id) as HTMLCanvasElement;
    if (chart_element) {
        let ctx = chart_element.getContext("2d");
        if (ctx) {
            var myChart = new Chart(ctx, {
                  type: chart_type
                , data: {
                      labels: labels
                    , datasets: datasets
                },
                options: {
                    legend: {
                        display: legend
                    }
                }
            });
            // avoid "'myChart' is declared but its value is never read" error message
            myChart = myChart;
        }
    }
}

function linkifyUrl(url:string) {
    if (url && url.startsWith("http")) {
        return "<a href='"+url+"' target='_blank'>"+url+"</a>";
    } else {
        return url;
    }
}

function outputStringArray(element_id: string, datasets:any){
    let output_element = document.getElementById(element_id);
    if (output_element) {
        let all_strings = "";
        let strings_by_language = getDatasetGivenLanguage(datasets, language);
        for (let index in strings_by_language) {
            all_strings += strings_by_language[index] + "<br>";
        }
        output_element.innerHTML = all_strings;
    }
}

function outputNoLangDoubleStringArray(element_id: string, datasets:any, break_string:string, post_string:string){
    let output_element = document.getElementById(element_id);
    if (output_element) {
        let all_strings = "";
        for (let index in datasets) {
            all_strings +=
                  Object.keys(datasets[index])[0]
                + break_string
                + Object.values(datasets[index])[0]
                + post_string;
        }
        output_element.innerHTML = all_strings;
    }
}

function adjust_data(dataset:any, adjustment_value:number){
    // function is currently not in use
    for (let index_dataset in dataset) {
        for (let index_data in dataset[index_dataset].data) {
            if (dataset[index_dataset].data[index_data] !== null) {
                dataset[index_dataset].data[index_data] = dataset[index_dataset].data[index_data]*adjustment_value
            }
        }
    }
    return dataset
}

function handle_local_anchors(){
    let full_url = window.location.href;
    if (full_url.includes('#')){
        let url_parts = full_url.split('#');
        if (url_parts.length > 1) {
            let archor_element = document.getElementById(url_parts[1]);
            if (archor_element) {
                archor_element.scrollIntoView();
            }
        }
    }
}

wwwService.getWebResource(url_db, function (response: string) {
    let response_json = JSON.parse(response);

    // nifty for debugging:
    //console.log( response_json.data );
    //console.log( JSON.stringify(response_json) );

    // ------
    // page_title
    // ------
    replaceElementWithString("vax.page_title", getTextGivenLanguage(response_json.data.page_title, language));

    // ------
    // page_title_2
    // ------
    replaceElementWithString("vax.page_title_2", getTextGivenLanguage(response_json.data.page_title_2, language));

    // ------
    // summary
    // ------
    let summary = document.getElementById("vax.summary.data");
    if (summary) {
        let percentage_processed = (response_json.data.key_data.total_processed_reports/response_json.data.key_data.total_reports) * 100;
        let percentage_processed_rounded = (Math.round(percentage_processed * 100) / 100).toFixed(2);
        let percentage_serious = (response_json.data.key_data.serious_side_effects_inc_death/response_json.data.key_data.total_processed_reports) * 100;
        let percentage_serious_rounded = (Math.round(percentage_serious * 100) / 100).toFixed(2);
        let this_title = getTextGivenLanguage(response_json.data.summary, language)
        .replace("[#total_reports]", response_json.data.key_data.total_reports)
        .replace("[#total_processed_reports]", response_json.data.key_data.total_processed_reports)
        .replace("[#total_processed_reports_pct]", percentage_processed_rounded)
        .replace("[#deaths]", response_json.data.key_data.deaths)
        .replace("[#deaths_below_60]", response_json.data.key_data.deaths_below_60)
        .replace("[#serious_side_effects_inc_death]", response_json.data.key_data.serious_side_effects_inc_death)
        .replace("[#serious_side_effects_inc_death_pct]", percentage_serious_rounded)
        ;
        summary.innerHTML = this_title;
    }

    // ------
    // report_page_last_updated_date
    // ------
    replaceElementWithString("vax.report_page_last_updated_date", getTextGivenLanguage(response_json.data.report_page_last_updated_date, language) + response_json.data.key_data.report_page_last_updated_date);

    // ------
    // information_message
    // ------
    replaceElementWithString("vax.information_message.title", getTextGivenLanguage(response_json.data.information_message.title, language));
    replaceElementWithString("vax.information_message.data", getTextGivenLanguage(response_json.data.information_message.datasets, language));

    // ------
    // deaths
    // ------
    // response_json.data.deaths.datasets = adjust_data(response_json.data.deaths.datasets, 10) // <!> if adjustment is needed, it can be done like this <!>
    replaceElementWithString("vax.deaths.title", response_json.data.key_data.deaths + " " + getTextGivenLanguage(response_json.data.deaths.title, language));
    replaceElementWithString("vax.deaths.sub_title", getTextGivenLanguage(response_json.data.deaths.sub_title, language));
    replaceElementWithString("vax.deaths.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.deaths.source));
    drawLineChart("vax.deaths.chart", response_json.data.labels_date_cutoff, response_json.data.deaths.datasets, true, true);

    // -----
    // deaths_below_60
    // -----
    replaceElementWithString("vax.deaths_below_60.title", response_json.data.key_data.deaths_below_60 + " " + getTextGivenLanguage(response_json.data.deaths_below_60.title, language));
    replaceElementWithString("vax.deaths_below_60.sub_title", getTextGivenLanguage(response_json.data.deaths_below_60.sub_title, language));
    replaceElementWithString("vax.deaths_below_60.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.deaths_below_60.source));
    drawLineChart("vax.deaths_below_60.chart", response_json.data.labels_date_cutoff, response_json.data.deaths_below_60.datasets, true, true);

    // -----
    // serious_side_effects_total
    // -----
    replaceElementWithString("vax.serious_side_effects_total.title", response_json.data.key_data.serious_side_effects_inc_death + " " + getTextGivenLanguage(response_json.data.serious_side_effects_total.title, language));
    replaceElementWithString("vax.serious_side_effects_total.sub_title", getTextGivenLanguage(response_json.data.serious_side_effects_total.sub_title, language));
    replaceElementWithString("vax.serious_side_effects_total.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.serious_side_effects_total.source));
    drawLineChart("vax.serious_side_effects_total.chart", response_json.data.labels_date_cutoff, response_json.data.serious_side_effects_total.datasets, true, true);

    // -----
    // serious_side_effects
    // -----
    replaceElementWithString("vax.serious_side_effects.title", response_json.data.key_data.serious_side_effects + " " + getTextGivenLanguage(response_json.data.serious_side_effects.title, language));
    replaceElementWithString("vax.serious_side_effects.sub_title", getTextGivenLanguage(response_json.data.serious_side_effects.sub_title, language));
    replaceElementWithString("vax.serious_side_effects.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.serious_side_effects.source));
    drawLineChart("vax.serious_side_effects.chart", response_json.data.labels_date_cutoff, response_json.data.serious_side_effects.datasets, true, true);

    // -----
    // other_less_serious_side_effects
    // -----
    replaceElementWithString("vax.other_less_serious_side_effects.title", response_json.data.key_data.other_less_serious_side_effects + " " + getTextGivenLanguage(response_json.data.other_less_serious_side_effects.title, language));
    replaceElementWithString("vax.other_less_serious_side_effects.sub_title", getTextGivenLanguage(response_json.data.other_less_serious_side_effects.sub_title, language));
    replaceElementWithString("vax.other_less_serious_side_effects.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.other_less_serious_side_effects.source));
    drawLineChart("vax.other_less_serious_side_effects.chart", response_json.data.labels_date_cutoff, response_json.data.other_less_serious_side_effects.datasets, true, true);

    // -----
    // serious_side_effects_by_age
    // -----
    replaceElementWithString("vax.serious_side_effects_by_age.title", response_json.data.key_data.serious_side_effects_inc_death + " " + getTextGivenLanguage(response_json.data.serious_side_effects_by_age.title, language));
    replaceElementWithString("vax.serious_side_effects_by_age.sub_title", getTextGivenLanguage(response_json.data.serious_side_effects_by_age.sub_title, language));
    replaceElementWithString("vax.serious_side_effects_by_age.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.serious_side_effects_by_age.source));
    drawBarOrPieChart("vax.serious_side_effects_by_age.chart", "bar", response_json.data.labels_age, response_json.data.serious_side_effects_by_age.datasets, false);

    // -----
    // all_side_effects_by_age
    // -----
    replaceElementWithString("vax.all_side_effects_by_age.title", response_json.data.key_data.total_processed_reports + " " + getTextGivenLanguage(response_json.data.all_side_effects_by_age.title, language));
    replaceElementWithString("vax.all_side_effects_by_age.sub_title", getTextGivenLanguage(response_json.data.all_side_effects_by_age.sub_title, language));
    replaceElementWithString("vax.all_side_effects_by_age.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.all_side_effects_by_age.source));
    drawBarOrPieChart("vax.all_side_effects_by_age.chart", "bar", response_json.data.labels_age, response_json.data.all_side_effects_by_age.datasets, false);

    // -----
    // all_side_effects_by_sex
    // -----
    replaceElementWithString("vax.all_side_effects_by_sex.title", response_json.data.key_data.total_processed_reports + " " + getTextGivenLanguage(response_json.data.all_side_effects_by_sex.title, language));
    replaceElementWithString("vax.all_side_effects_by_sex.sub_title", getTextGivenLanguage(response_json.data.all_side_effects_by_sex.sub_title, language));
    replaceElementWithString("vax.all_side_effects_by_sex.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.all_side_effects_by_sex.source));
    drawBarOrPieChart("vax.all_side_effects_by_sex.chart", "pie", response_json.data.labels_sex, response_json.data.all_side_effects_by_sex.datasets, false);

    // -----
    // total_reports
    // -----
    if (document.getElementById("vax.total_reports.title")) {
        let percentage = 100 - (response_json.data.key_data.total_processed_reports/response_json.data.key_data.total_reports) * 100;
        let percentage_rounded = (Math.round(percentage * 100) / 100).toFixed(2);
        let unprocessed_reports = (response_json.data.key_data.total_reports - response_json.data.key_data.total_processed_reports).toFixed();
        let this_title = getTextGivenLanguage(response_json.data.total_reports.title, language)
            .replace("[#1]", response_json.data.key_data.total_reports)
            .replace("[#2]", unprocessed_reports)
            .replace("[#3]", percentage_rounded);

        replaceElementWithString("vax.total_reports.title", this_title);
        replaceElementWithString("vax.total_reports.sub_title", getTextGivenLanguage(response_json.data.total_reports.sub_title, language));
        replaceElementWithString("vax.total_reports.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.total_reports.source));
    }
    drawLineChart("vax.total_reports.chart", response_json.data.labels_date_cutoff, response_json.data.total_reports.datasets, false, true);

    // -----
    // total_reports_by_manufacturer
    // -----
    replaceElementWithString("vax.total_reports_by_manufacturer.title", response_json.data.key_data.total_reports_by_manufacturer + " " + getTextGivenLanguage(response_json.data.total_reports_by_manufacturer.title, language));
    replaceElementWithString("vax.total_reports_by_manufacturer.sub_title", getTextGivenLanguage(response_json.data.total_reports_by_manufacturer.sub_title, language));
    replaceElementWithString("vax.total_reports_by_manufacturer.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.total_reports_by_manufacturer.source));
    drawLineChart("vax.total_reports_by_manufacturer.chart", response_json.data.labels_date_cutoff, response_json.data.total_reports_by_manufacturer.datasets, false, true);

    // -----
    // percentage_with_reported_side_effects
    // -----
    replaceElementWithString("vax.percentage_with_reported_side_effects.title", response_json.data.key_data.percentage_with_reported_side_effects + " " + getTextGivenLanguage(response_json.data.percentage_with_reported_side_effects.title, language));
    replaceElementWithString("vax.percentage_with_reported_side_effects.sub_title", getTextGivenLanguage(response_json.data.percentage_with_reported_side_effects.sub_title, language));
    replaceElementWithString("vax.percentage_with_reported_side_effects.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.percentage_with_reported_side_effects.source));
    drawLineChart("vax.percentage_with_reported_side_effects.chart", response_json.data.labels_date_cutoff, response_json.data.percentage_with_reported_side_effects.datasets, false, true);

    // -----
    // total_vaccinated
    // -----
    if (document.getElementById("vax.total_vaccinated.title")) {
        let gap = Math.abs(response_json.data.key_data.vaccinated_dose_2 - response_json.data.key_data.vaccinated_dose_1);
        let this_title = getTextGivenLanguage(response_json.data.total_vaccinated.title, language)
            .replace("[#1]", response_json.data.key_data.vaccinated_dose_1)
            .replace("[#2]", response_json.data.key_data.vaccinated_dose_2)
            .replace("[#3]", String(gap));

            replaceElementWithString("vax.total_vaccinated.title", this_title);
            replaceElementWithString("vax.total_vaccinated.sub_title", getTextGivenLanguage(response_json.data.total_vaccinated.sub_title, language));
            replaceElementWithString("vax.total_vaccinated.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.total_vaccinated.source));
    }
    drawLineChart("vax.total_vaccinated.chart", response_json.data.labels_date, response_json.data.total_vaccinated.datasets, false, true);

    // -----
    // covid_deaths_per_week
    // -----
    replaceElementWithString("vax.covid_deaths_per_week.title", response_json.data.key_data.covid_associated_deaths + " " + getTextGivenLanguage(response_json.data.covid_deaths_per_week.title, language));
    replaceElementWithString("vax.covid_deaths_per_week.sub_title", getTextGivenLanguage(response_json.data.covid_deaths_per_week.sub_title, language));
    replaceElementWithString("vax.covid_deaths_per_week.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.covid_deaths_per_week.source));
    drawBarOrPieChart("vax.covid_deaths_per_week.chart", "bar", response_json.data.labels_week2, response_json.data.covid_deaths_per_week.datasets, false);

    // -----
    // c19_test_results
    // -----
    replaceElementWithString("vax.c19_test_results.title", response_json.data.key_data.covid_positive_tests + " " + getTextGivenLanguage(response_json.data.c19_test_results.title, language));
    replaceElementWithString("vax.c19_test_results.sub_title", getTextGivenLanguage(response_json.data.c19_test_results.sub_title, language));
    replaceElementWithString("vax.c19_test_results.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.c19_test_results.source));
    drawBarOrPieChart("vax.c19_test_results.chart", "bar", response_json.data.labels_days_all_pandemic, response_json.data.c19_test_results.datasets, false);

    // -----
    // total_covid_deaths_by_vax_status
    // -----
    replaceElementWithString("vax.total_covid_deaths_by_vax_status.title", getTextGivenLanguage(response_json.data.total_covid_deaths_by_vax_status.title, language));
    replaceElementWithString("vax.total_covid_deaths_by_vax_status.sub_title", getTextGivenLanguage(response_json.data.total_covid_deaths_by_vax_status.sub_title, language));
    replaceElementWithString("vax.total_covid_deaths_by_vax_status.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.total_covid_deaths_by_vax_status.source));
    drawLineChart("vax.total_covid_deaths_by_vax_status.chart", response_json.data.labels_week3, response_json.data.total_covid_deaths_by_vax_status.datasets, false, true);

    // -----
    // breakthrough_hospitalisations
    // -----
    replaceElementWithString("vax.breakthrough_hospitalisations.title", getTextGivenLanguage(response_json.data.breakthrough_hospitalisations.title, language).replace("[#1]", response_json.data.key_data.breakthrough_hospitalisations));
    replaceElementWithString("vax.breakthrough_hospitalisations.sub_title", getTextGivenLanguage(response_json.data.breakthrough_hospitalisations.sub_title, language));
    replaceElementWithString("vax.breakthrough_hospitalisations.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.breakthrough_hospitalisations.source));
    drawLineChart("vax.breakthrough_hospitalisations.chart", response_json.data.labels_week4, response_json.data.breakthrough_hospitalisations.datasets, false, true);

    // -----
    // total_deaths_per_week
    // -----
    replaceElementWithString("vax.total_deaths_per_week.title", getTextGivenLanguage(response_json.data.total_deaths_per_week.title, language));
    replaceElementWithString("vax.total_deaths_per_week.sub_title", getTextGivenLanguage(response_json.data.total_deaths_per_week.sub_title, language));
    replaceElementWithString("vax.total_deaths_per_week.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.total_deaths_per_week.source));
    drawLineChart("vax.total_deaths_per_week.chart", response_json.data.labels_week, response_json.data.total_deaths_per_week.datasets, false, true);

    // -----
    // avg_deaths_per_week
    // -----
    replaceElementWithString("vax.avg_deaths_per_week.title", getTextGivenLanguage(response_json.data.avg_deaths_per_week.title, language));
    replaceElementWithString("vax.avg_deaths_per_week.sub_title", getTextGivenLanguage(response_json.data.avg_deaths_per_week.sub_title, language));
    replaceElementWithString("vax.avg_deaths_per_week.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.avg_deaths_per_week.source));
    drawLineChart("vax.avg_deaths_per_week.chart", response_json.data.labels_week, response_json.data.avg_deaths_per_week.datasets, false, true);

    // -----
    // yearly_side_effects_reports
    // -----
    replaceElementWithString("vax.yearly_side_effects_reports.title", getTextGivenLanguage(response_json.data.yearly_side_effects_reports.title, language));
    replaceElementWithString("vax.yearly_side_effects_reports.sub_title", getTextGivenLanguage(response_json.data.yearly_side_effects_reports.sub_title, language));
    replaceElementWithString("vax.yearly_side_effects_reports.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.yearly_side_effects_reports.source));
    drawLineChart("vax.yearly_side_effects_reports.chart", response_json.data.labels_year, response_json.data.yearly_side_effects_reports.datasets, false, true);

    // -----
    // yearly_reported_deaths_from_medicines
    // -----
    replaceElementWithString("vax.yearly_reported_deaths_from_medicines.title", getTextGivenLanguage(response_json.data.yearly_reported_deaths_from_medicines.title, language));
    replaceElementWithString("vax.yearly_reported_deaths_from_medicines.sub_title", getTextGivenLanguage(response_json.data.yearly_reported_deaths_from_medicines.sub_title, language));
    replaceElementWithString("vax.yearly_reported_deaths_from_medicines.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.yearly_reported_deaths_from_medicines.source));
    drawLineChart("vax.yearly_reported_deaths_from_medicines.chart", response_json.data.labels_year, response_json.data.yearly_reported_deaths_from_medicines.datasets, false, true);

    // -----
    // causes_of_death
    // -----
    replaceElementWithString("vax.causes_of_death.title", getTextGivenLanguage(response_json.data.causes_of_death.title, language));
    replaceElementWithString("vax.causes_of_death.sub_title", getTextGivenLanguage(response_json.data.causes_of_death.sub_title, language));
    replaceElementWithString("vax.causes_of_death.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.causes_of_death.source));
    drawLineChart("vax.causes_of_death.chart", response_json.data.labels_year2, response_json.data.causes_of_death.datasets, false, true);

    // -----
    // total_hospitalised_flu_patients
    // -----
    replaceElementWithString("vax.total_hospitalised_flu_patients.title", getTextGivenLanguage(response_json.data.total_hospitalised_flu_patients.title, language));
    replaceElementWithString("vax.total_hospitalised_flu_patients.sub_title", getTextGivenLanguage(response_json.data.total_hospitalised_flu_patients.sub_title, language));
    replaceElementWithString("vax.total_hospitalised_flu_patients.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.total_hospitalised_flu_patients.source));
    drawLineChart("vax.total_hospitalised_flu_patients.chart", response_json.data.labels_year3, response_json.data.total_hospitalised_flu_patients.datasets, false, true);

    // -----
    // total_hospitalised_respiratory_patients
    // -----
    replaceElementWithString("vax.total_hospitalised_respiratory_patients.title", getTextGivenLanguage(response_json.data.total_hospitalised_respiratory_patients.title, language));
    replaceElementWithString("vax.total_hospitalised_respiratory_patients.sub_title", getTextGivenLanguage(response_json.data.total_hospitalised_respiratory_patients.sub_title, language));
    replaceElementWithString("vax.total_hospitalised_respiratory_patients.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.total_hospitalised_respiratory_patients.source));
    drawLineChart("vax.total_hospitalised_respiratory_patients.chart", response_json.data.labels_year3, response_json.data.total_hospitalised_respiratory_patients.datasets, false, true);

    // -----
    // total_hospitalised_patients
    // -----
    replaceElementWithString("vax.total_hospitalised_patients.title", getTextGivenLanguage(response_json.data.total_hospitalised_patients.title, language));
    replaceElementWithString("vax.total_hospitalised_patients.sub_title", getTextGivenLanguage(response_json.data.total_hospitalised_patients.sub_title, language));
    replaceElementWithString("vax.total_hospitalised_patients.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.total_hospitalised_patients.source));
    drawLineChart("vax.total_hospitalised_patients.chart", response_json.data.labels_year3, response_json.data.total_hospitalised_patients.datasets, false, true);

    // -----
    // sick_leave
    // -----
    replaceElementWithString("vax.sick_leave.title", getTextGivenLanguage(response_json.data.sick_leave.title, language));
    replaceElementWithString("vax.sick_leave.sub_title", getTextGivenLanguage(response_json.data.sick_leave.sub_title, language));
    replaceElementWithString("vax.sick_leave.source", getTextGivenLanguage(response_json.data.source_text, language) + linkifyUrl(response_json.data.sick_leave.source));
    drawLineChart("vax.sick_leave.chart", response_json.data.labels_year_quarter, response_json.data.sick_leave.datasets, false, true);

    // ------
    // related_media
    // ------
    replaceElementWithString("vax.related_media.title", getTextGivenLanguage(response_json.data.related_media.title, language));
    outputNoLangDoubleStringArray("vax.related_media.data", response_json.data.related_media.datasets, "<br>", "<br><br>");

    // ------
    // related_news
    // ------
    replaceElementWithString("vax.related_news.title", getTextGivenLanguage(response_json.data.related_news.title, language));
    replaceElementWithString("vax.related_news.sub_title", getTextGivenLanguage(response_json.data.related_news.sub_title, language));
    outputNoLangDoubleStringArray("vax.related_news.data", response_json.data.related_news.datasets, ": ", "<br>");

    // ------
    // related_resources
    // ------
    replaceElementWithString("vax.related_resources.title", getTextGivenLanguage(response_json.data.related_resources.title, language));
    outputNoLangDoubleStringArray("vax.related_resources.data", response_json.data.related_resources.datasets, ": ", "<br>");

    // ------
    // comments
    // ------
    replaceElementWithString("vax.comments.title", getTextGivenLanguage(response_json.data.comments.title, language));
    outputStringArray("vax.comments.data", response_json.data.comments.datasets);

    // ------
    // credits
    // ------
    replaceElementWithString("vax.credits.title", getTextGivenLanguage(response_json.data.credits.title, language));
    outputStringArray("vax.credits.data", response_json.data.credits.datasets);

    // ------
    // handle anchor/links to same page
    // ------
    handle_local_anchors();

});
