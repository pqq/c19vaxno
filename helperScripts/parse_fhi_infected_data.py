#!/usr/bin/python
# pylint: disable-msg=too-many-locals, too-many-branches, too-many-statements, line-too-long

"""
parse fhi infection data
by Frode B K (aka. Nzwlm Jczlit Stmdabct)

- open https://www.fhi.no/sv/smittsomme-sykdommer/corona/dags--og-ukerapporter/dags--og-ukerapporter-om-koronavirus/
- open the table "Antall personer testet for covid-19 per dag og andel positive blant disse"
- mark all the content in that table
- paste that content into a new file on your hd, for example named "tmp.txt"
- run this script on that file, for example 'python parse_fhi_infected_data.py -i tmp.txt'
-
- command in use:
- python /media/drive2/nass/cb/gitRepos/gitlab/c19vaxno/helperScripts/parse_fhi_infected_data.py -i /media/drive2/nass/cb/gitRepos/gitlab/c19vaxno/dataSources/fhi_dagsrapporter/2022/table_data.txt
"""

import sys
import getopt
import os
import pathlib


def main(argv):
    """
    main
    """
    script_name = os.path.basename(__file__)
    input_file = ''
    fhi_date = []
    fhi_negative = []
    fhi_positive = []
    fhi_pct = []
    fhi_positive_total = 0
    fhi_negative_total = 0


    try:
        opts, _ = getopt.getopt(argv,"hi:",["ifile="])
    except getopt.GetoptError:
        print(script_name + ' -i <inputfile>')
        sys.exit(2)

    if len(opts) == 0:
        print(script_name + ' -i <inputfile>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(script_name + ' -i <inputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg

    input_dir = str(pathlib.Path(input_file).parent.resolve())

    print('---')
    print('input_file: ' + input_file)
    print('input_dir: ' + input_dir)

    with open(input_file, mode='r', encoding='utf-8') as file:
        while line := file.readline():
            (_fhi_date, _fhi_negative, _fhi_positive, _fhi_pct) = line.split('\t')
            fhi_date.append(_fhi_date)
            fhi_negative.append(_fhi_negative)
            fhi_positive.append(_fhi_positive)
            fhi_pct.append(_fhi_pct)

    print('\n===== DATE ====')
    fhi_date.reverse()
    first_iteration = True
    for _date in fhi_date:
        if first_iteration:
            first_iteration = False
        else:
            print(',', end='')
        print('"'+_date+'"', end='')

    print('\n\n===== NEGATIVE ====')
    fhi_negative.reverse()
    first_iteration = True
    for _negative in fhi_negative:
        fhi_negative_total += int(_negative)
        if first_iteration:
            first_iteration = False
        else:
            print(',', end='')
        print(_negative, end='')

    print('\n\nTOTAL NEGATIVE: ' + str(fhi_negative_total))

    print('\n\n===== POSITIVE ====')
    fhi_positive.reverse()
    first_iteration = True
    for _positive in fhi_positive:
        fhi_positive_total += int(_positive)
        if first_iteration:
            first_iteration = False
        else:
            print(',', end='')
        print(_positive, end='')

    print('\n\nTOTAL POSITIVE: ' + str(fhi_positive_total))

    print()


if __name__ == "__main__":
    main(sys.argv[1:])
