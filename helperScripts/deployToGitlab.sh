#!/usr/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# what:        deploy app to pqq.gitlab.io
# author:      njs
# started:     nov 2021
#
# requires:    n/a
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


if [ "$EUID" -eq 0 ]
  then echo "error: do not run as 'root'"
  exit
fi

src_dir=/media/drive2/nass/cb/gitRepos/gitlab/c19vaxno/src/app
trg_dir=/media/drive2/nass/cb/gitRepos/gitlab/pqq.gitlab.io/public/c19vaxno

if ! [ -d "$src_dir" ]
then
  echo "missing source directory:"
  echo "$src_dir"
  exit
fi

if ! [ -d "$trg_dir" ]
then
  echo "missing target directory:"
  echo "$trg_dir"
  exit
fi

echo copying files to $trg_dir...
cp -r --verbose $src_dir/* "$trg_dir/"
