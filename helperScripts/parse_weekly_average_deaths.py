#!/usr/bin/python
# pylint: disable-msg=too-many-locals, too-many-branches, too-many-statements, line-too-long

"""
parse weekly average
by Frode Burdal Klevstul

$ python /media/drive2/nass/cb/gitRepos/gitlab/c19vaxno/helperScripts/parse_weekly_average_deaths.py -i /media/drive2/nass/cb/gitRepos/gitlab/c19vaxno/dataSources/ssb_doedsfallUke/misc/weekly_average_deaths.txt
"""

import sys
import getopt
import os


def main(argv):
    """
    main
    """
    script_name = os.path.basename(__file__)
    input_file = ''
    data_avg = []
    data_2020 = []
    data_2021 = []
    data_2022 = []

    try:
        opts, _ = getopt.getopt(argv,"hi:",["ifile="])
    except getopt.GetoptError:
        print(script_name + ' -i <inputfile>')
        sys.exit(2)

    if len(opts) == 0:
        print(script_name + ' -i <inputfile>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(script_name + ' -i <inputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg

    print('---')
    print('input_file: ' + input_file)

    with open(input_file, mode='r', encoding='utf-8') as file:
        while line := file.readline():
            (_data_avg, _data_2020, _data_2021, _data_2022) = line.split()
            if _data_avg.isdigit():
                data_avg.append(int(_data_avg))
                data_2020.append(int(_data_2020))
                data_2021.append(int(_data_2021))
                data_2022.append(int(_data_2022))

    print('\n===== 2020 ====')
    first_iteration = True
    i = 0
    for _data_avg in data_avg:
        if first_iteration:
            first_iteration = False
        else:
            print(',', end='')
        print( round(((data_2020[i]/_data_avg)-1)*100, 2), end='')
        i += 1

    print('\n===== 2021 ====')
    first_iteration = True
    i = 0
    for _data_avg in data_avg:
        if first_iteration:
            first_iteration = False
        else:
            print(',', end='')
        print( round(((data_2021[i]/_data_avg)-1)*100, 2), end='')
        i += 1

    print('\n===== 2022 ====')
    first_iteration = True
    i = 0
    for _data_avg in data_avg:
        if first_iteration:
            first_iteration = False
        else:
            print(',', end='')
        print( round(((data_2022[i]/_data_avg)-1)*100, 2), end='')
        i += 1

    print()


if __name__ == "__main__":
    main(sys.argv[1:])
