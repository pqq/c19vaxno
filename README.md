# c19vaxno

## COVID-19 Vaccination Status for Norway

"c19vaxno" is an open source project, that keeps track of side effects from the COVID-19 vaccination in Norway. This solution is using nothing but [official data sources](https://gitlab.com/pqq/c19vaxno/-/tree/main/dataSources).


# Online version

- This solution is up and running at [pqq.gitlab.io/c19vaxno](https://pqq.gitlab.io/c19vaxno/)
- The database is hosted at [https://pqq.gitlab.io/c19vaxno/db/database.json](https://pqq.gitlab.io/c19vaxno/db/database.json)


# Use and integration

Feel free to use this solution, as you like, according to [the licence](https://gitlab.com/pqq/c19vaxno/-/blob/main/LICENCE.txt).

## Self hosted

Download [the app](https://gitlab.com/pqq/c19vaxno/-/tree/main/src/app), and host it yourself. If you do this, remember to keep updating [the database file]((https://gitlab.com/pqq/c19vaxno/-/blob/main/src/app/db/database.json)).

## JavaScript Widgets

On an HTML page, you can include JavaScript and HTML code, to integrate this solution as different widgets. You do this by integrating the solution, running at [pqq.gitlab.io/c19vaxno](https://pqq.gitlab.io/c19vaxno/). For an example, [see this page](https://gitlab.com/pqq/c19vaxno/-/blob/main/test/remote_test/index.html). An overview of all charts and available data [is found here](https://gitlab.com/pqq/c19vaxno/-/blob/main/src/app/index.html).

### WordPress

1. Install the [HFCM (Header Footer Code Manager) plugin](https://wordpress.org/plugins/header-footer-code-manager/).  

2. Create a page, or post, to present the statistics.  

3. Create a snippet in HMCF, for the specific page(s)/post(s) where the stat is to be presented (language options: {en,no}, for English or Norwegian). Below code is setting the language to "no" for Norwegian:  
```
<script src="https://pqq.gitlab.io/c19vaxno/Chart.js/Chart.min.js"></script>
<script>
(function () {
    sessionStorage.setItem('vax.language', 'no');
})();
</script>
```

4. Open code editor for the given page/post, and [add any available](https://gitlab.com/pqq/c19vaxno/-/blob/main/src/app/index.html) chart or data, like:  
```
<!-- deaths -->
<div class="container">
    <h1 id="vax.deaths.title">Loading...</h1>
    <canvas id="vax.deaths.chart" width="100" height="100"></canvas>
</div>
<p><script type="module" src="https://pqq.gitlab.io/c19vaxno/js/archi.js"></script></p>
```


# 3rd party resources

- [chart.js](https://www.chartjs.org/)
- [favicon](https://www.favicon.cc/?action=icon&file_id=869213) (used @ gitlab.io)
- [Background image: anna shvets](https://www.pexels.com/photo/person-holding-syringe-3786215/) (used @ gitlab.io)


# Database

- [@gitlab.com](https://gitlab.com/pqq/c19vaxno/-/blob/main/src/app/db/database.json)
- [@gitlab.io](https://pqq.gitlab.io/c19vaxno/db/database.json)


# Data sources

- [Downloaded copies of all reports](https://gitlab.com/pqq/c19vaxno/-/tree/main/dataSources)


# Found an error or a bug?

- [Create an issue](https://gitlab.com/pqq/c19vaxno/-/issues)
